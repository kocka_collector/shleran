# Shleran (wip)

A set of tools to help a shell predict what you want

## the idea

While a number of tools exist to help one repeat commands from one's shell
history (such as fzf's ^r bash-completion shortcut), often, one runs a set of
commands, not just one. After I `cd` into a git repo, I might almost always
want to `git pull`. After a program segfaults, one might want to run a debugger
on it. Conventional history tools work independent of pretty much all context.
If I `cd` into a folder, fzf will quite happily recommend I repeat the same
relative cd.

Shleran plans to address this issue by learnign from not just the information in
your `~/.bash_history`, but also the directory the command was run in, and it's
return value.

## usage

Currently, shleran can only gather training data. However, the format used
should not change, so you can go ahead and add `source path/to/shleran/learn.sh`
to your `.bashrc`. This will save the information it uses to a file (currently
`~/.config/shleran-training`), in a format described later. If you want to read
the data, use the `shleran_pretty_print` command, which will print the data in a
tab-separated table. Do be aware, that commands and directory names may contain
pretty much any whitespace, so if you want to parse the data, don't use the
pretty-printer.

## data format

Every command is saved along with the working directory, and it's return value.
Each of these fields is terminated by a null character (as neither file names,
not command lines may contain that character). In case other fields are added in
the future, each of these records is terminated with the ASCII RS (record
separator) character, aka \x1E.

## the plan/ TODO

 -[x] Save the data to learn from, and keep this information updated.
   - output the training data in a human-readable format
 -[ ] find/write an algorithm that finds the k most probable next commands,
 based on the training data
 -[ ] add a shortcut to let the user actually use the recommendations
 -[ ] make it work on non-bash shells


