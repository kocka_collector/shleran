#!/bin/bash

shleran_training_file="$HOME/.config/shleran-training"

shleran_save_training_data(){
	local retval=$?
	local command=$(history 1|sed 's/^ [0-9]*  //')
	local pwd=$(pwd)
	printf '%s\0%s\0%s\0\x1e' "$pwd" "$command" "$retval" >> $shleran_training_file
	return $retval
}

PROMPT_COMMAND="shleran_save_training_data;$PROMPT_COMMAND"

shleran_pretty_print(){
	awk 'BEGIN { RS="\x1e"; FS="\0" } 
		{ 
			for (i = 1; i <= NF; i++) {
				printf "%s", $i;
				if (i < NF) printf "\t";  # Replace null bytes with tabs
			}
			 printf "\n";  # Replace RS characters with new lines
		}' $shleran_training_file
}
